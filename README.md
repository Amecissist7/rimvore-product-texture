# RimVore-Product Texture

A retexturizing mod for 

Rimvore-2 https://gitlab.com/Nabber/rimvore-2 and

Rimvore-overstuffed https://gitlab.com/MamaHorseteeth/rimvore-2-overstuffed

that directs efforts at integrating into vanilla!